import { Request, Response } from "express";
import { InteresRevision } from "../models/interes_revision";


export default class InteresesController {

    static obtenerIntereses = async (req: Request, res: Response) => {
        try {
            const intereses = await InteresRevision.findAll();
             res.status(200).json(intereses);
        } catch (error) {
            console.log(error)
            res.status(500);
        }
        
    }

    static guardarInteres = async (req: Request, res: Response) => {
        const {nombre} = req.body;

        const newPostulacion = await InteresRevision.create({
            nombre : nombre
        });

        res.json(newPostulacion);
    }

   

}
