import { Request, Response } from "express";
import { Documento } from "../models/documento";

import { Postulacion } from "../models/postulacion";

//import multer from "multer";

//const upload = multer({dest : 'uploads'})

//import { Postulacion } from "../models/postulacion";

export default class PostulacionController {

    static save = async (req: Request, res: Response) => {
        const {title, userId} = req.body;

        const fileDetails =  JSON.parse( req.body.fileDetails);

       // console.log('TITLE ->', title)

        const files : any = req.files;
        
        console.log('DETAILS ->', fileDetails)
        console.log('FILES ->', files)
        
        

        //guarda datos de postulacion
       const newPostulacion = await Postulacion.create({
            title : title,
            userId : userId
        });

        for (let i = 0; i < fileDetails.length; i++) {
            for (let j = 0; j < files.length; j++)  {
                console.log('ENCONTRADOS', files[j].originalname.normalize("NFD").replace(/[\u0300-\u036f]/g, ""))
                if(files[j].originalname == fileDetails[i].name){
                    console.log('ENCONTRADOS', files[j].originalname.normalize("NFD").replace(/[\u0300-\u036f]/g, ""))

                    console.log('NAME -> ',files[j].originalname ,'TYPE ->' , fileDetails[i].type )
                     Documento.create({
                        tipo : fileDetails[i].type,
                        postulacionId : newPostulacion.id,
                        source : files[j].destination + files[j].filename
        
                    })
                }
                
            }
            
        }


        //guarda archivos relacionados a la postulacion

     
        
        /*
            files.forEach(async (file : any) =>  {


                await Documento.create({
                    tipo : 'anexo',
                    postulacionId : newPostulacion.id,
                    source : file.destination + file.filename
    
                })
            }); */
        
        

        
        res.json('ok');
    }

}