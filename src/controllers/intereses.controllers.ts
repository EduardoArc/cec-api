import { Request, Response } from "express";
import { InteresRevision } from "../models/interes_revision";

//Listar
export const obtenerIntereses = async (req: Request, res: Response) => {

    try {
        const intereses = await InteresRevision.findAll({
            where: {
              estado: true
            }
          });

        res.status(200).json({
            
            message:'Listado de intereses de revisión',
            data : intereses

        });
    } catch (error) {
        res.status(500).json({message: 'Error al obtener listado de intereses de revisión'})
    }

    


}

//Obtener
export const obtenerInteres = async (req: Request, res: Response) => {
    try {
        const {id} = req.params;
        const interes = await InteresRevision.findByPk(id);
        if(interes){
            res.status(200).json({
                message : 'Interés obtenido',
                data: interes
            })
        }

    } catch (error) {
        
    }
}

//Crear
export const crearInteres = async (req: Request , res : Response) =>{
    try {

        const {nombre} = req.body;

        if(nombre != null && nombre != ''){
            const interes = await InteresRevision.create({nombre});

            res.status(200).json({
                message: 'El interés de revisión de ha creado correctamente',
                data: interes
            })

            

        }else{
            res.status(402);
        }
        
        
    } catch (error) {
        console.log('ERROR -> ',error)
        res.status(501);
    }
}

export const editarInteres = async (req: Request , res : Response) =>{
    try {
        const {id} = req.params;
        const {nombre} = req.body;

        const interes = await InteresRevision.findByPk(id);
    
        await interes?.update({nombre});

        res.status(200).json({
            message : 'El interés de revisión ha sido editado correctamente',
            data: interes
        })



    } catch (error) {
        
    }
}

export const deshabilitarInteres = async (req: Request, res : Response) => {
    try {
        const {id} = req.params;
        const interes = await InteresRevision.findByPk(id);

        await interes?.update({estado : false});

        res.status(200).json({
            message : 'El interés de revisión ha sido deshabilitado correctamente',
            data: interes
        })
        
    } catch (error) {
        
    }
    


}

export const habilitarInteres = async (req: Request, res : Response) => {
    try {
        const {id} = req.params;
        const interes = await InteresRevision.findByPk(id);

        await interes?.update({estado : true});

        res.status(200).json({
            message : 'El interés de revisión ha sido deshabilitado correctamente',
            data: interes
        })
        
    } catch (error) {
        
    }
}


