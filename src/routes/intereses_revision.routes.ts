import { Router } from "express";
import { obtenerIntereses, obtenerInteres, crearInteres, editarInteres, deshabilitarInteres, habilitarInteres } from "../controllers/intereses.controllers";


import InteresesController from "../controllers/intereses_bkp.controllers";
const router = Router();

router.get('/intereses' ,obtenerIntereses ); //Listar
router.get('/intereses/:id', obtenerInteres);//Obtener
router.post('/intereses', crearInteres); //Crear
router.put('/intereses/:id', editarInteres); //Editar
router.delete('/intereses/:id', deshabilitarInteres); //deshabilitar
router.get('/intereses/habilitar/:id', habilitarInteres)


//router.post('/intereses', InteresesController.guardarInteres );


export default router;