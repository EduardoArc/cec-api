import {Table,Model,Column,DataType, BelongsTo, ForeignKey, HasMany} from 'sequelize-typescript';
import { Documento } from './documento';
import { User } from './user';

@Table({
    timestamps:false,
    tableName: "postulacion"
})

export class Postulacion extends Model {

    @Column({
        type: DataType.INTEGER,
        primaryKey:true,
        allowNull:false,
        autoIncrement:true
    })
    id!:number;

    @Column({
        type: DataType.STRING,
        allowNull:true
    })
    title!:string;

    @HasMany(() => Documento)
    postulaciones?: Documento

    
    //declara id extranjera
    @ForeignKey(() => User)
    @Column
    userId!: number

    //declara relación pertenece a..
    @BelongsTo(()=>User)
    user!: User

}


