import {Table,Model,Column,DataType, BelongsTo, ForeignKey} from 'sequelize-typescript';
import { Postulacion } from './postulacion';

@Table({
    timestamps:false,
    tableName: "documento"
})

export class Documento extends Model {

    @Column({
        type: DataType.INTEGER,
        primaryKey:true,
        allowNull:false,
        autoIncrement:true
    })
    id!:number;

    @Column({
        type: DataType.STRING,
        allowNull:true
    })
    tipo!:string;

    @Column({
        type: DataType.STRING,
        allowNull:true
    })
    source!:string;

    @ForeignKey(() => Postulacion)
    @Column
    postulacionId!: number


    @BelongsTo(()=>Postulacion)
    postulacion!: Postulacion

}