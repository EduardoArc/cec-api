import {Table,Model,Column,DataType, BelongsTo, ForeignKey} from 'sequelize-typescript';


@Table({
    timestamps:false,
    tableName: "interes_revision"
})

export class InteresRevision extends Model {

    @Column({
        type: DataType.INTEGER,
        primaryKey:true,
        allowNull:false,
        autoIncrement:true
    })
    id!:number;

    @Column({
        type: DataType.STRING,
        allowNull:true,
        unique: true,
        
    })
    nombre!:string;

    

    @Column({
        type: DataType.BOOLEAN,
        allowNull:true,
        defaultValue: true
    })
    estado!:boolean;


}