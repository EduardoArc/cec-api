import {Table,Model,Column,DataType, BelongsTo, ForeignKey} from 'sequelize-typescript';


@Table({
    timestamps:false,
    tableName: "institucion"
})

export class Institucion extends Model {

    @Column({
        type: DataType.INTEGER,
        primaryKey:true,
        allowNull:false,
        autoIncrement:true
    })
    id!:number;

    @Column({
        type: DataType.STRING,
        allowNull:true
    })
    nombre!:string;

    @Column({
        type: DataType.BOOLEAN,
        allowNull:true,
        defaultValue: false
    })
    estado!:boolean;


}