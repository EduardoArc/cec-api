import { Sequelize } from 'sequelize-typescript';
import { Documento } from '../models/documento';
import { Institucion } from '../models/institucion';
import { InteresRevision } from '../models/interes_revision';
import { Postulacion } from '../models/postulacion';
import { RolInterno } from '../models/rol_interno';
import { User } from '../models/user';

export const conection = new Sequelize({
    host: 'localhost',
    username: 'eduardoarce',
    database:'CEC_DB',
    dialect: 'postgres',
    password: '',
    models:[
      User, 
      Postulacion, 
      Documento, 
      InteresRevision,
      RolInterno,
      Institucion
    ],
    //Evita mostrar mensaje en consola
    logging:true
  });